#!/usr/bin/python
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient
import boto3, botocore
import datetime
from pytz import timezone
import pytz
import unicodedata ###To convert aws s3 object key from unicode to ascii format
import gzip ### To decompress gz files downloaded from s3 bucket
import shutil ### convenient helper for copying content from one file-like object to another
import os, csv, json ### necessary for jsonconversion() function
import StringIO ### necessary for jsonconversion() function

### remember to do: pip install boto3, botocore, pytz
### create ~/.aws/credentials file and save aws_access_key_id and aws_secret_access_key

###format for credentials file:

'''
[default]
aws_access_key_id = <accesskey>
aws_secret_access_key = <secretkey>

'''


### This is to get the total seconds passed from epoch and ignore the offset coming naturally with aws s3 timestamp

def convertS3Timestamp(dt):
    epoch = datetime.datetime.utcfromtimestamp(0) ### epoch is '1970-01-01 00:00:00'
    nativetime = dt.replace(tzinfo=None) ### To ignore the offset
    totalSeconds = int((nativetime - epoch).total_seconds()) ## To show how many seconds from epoch passed
    return totalSeconds

def jsonconversion(filefeed):
    with open("/var/log/i-platinum/decompressed/"+filefeed) as f:
        rawlog = f.read()

    rawlog = rawlog.replace(" +0000","")

    fixlog = StringIO.StringIO()
    fixlog.write(rawlog)
    fixlog.seek(0)
    thecsv = csv.reader(fixlog, delimiter=' ', quotechar='"')
    fieldnames=["Client_IP","User_Name","Local_Time","Request_Line","Status","Size","Referer","User_Agent","Platinum_Optimizations","Platinum_Metrics","Content_Type","X-Forwarded-Forwarded-For","Handling","Time_First_Byte","Forwarded_Protocol","UUID","FW_Flag","OS_Response_Code","Optional_WAF_fields","FW_Info","Site_ADN_ID","Rule_ID","SystemADN_ID"]
    fixlog.seek(0)
    csvdict = csv.DictReader(fixlog, fieldnames, delimiter=' ', quotechar='"')

    out = json.dumps( [ row for row in csvdict ] )

    with open("/var/log/i-platinum/json/"+filefeed, 'w') as f:
        f.write(out)

### Go through all files under /var/log/i-platinum/decompressed/, if they end with .gz convert them to json using jsonconversion() function

### if path changes make sure the script will have write access right to the new location
def upload_to_azure(filetoupload):
    try:
        connect_str = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
        blob_service_client = BlobServiceClient.from_connection_string(connect_str)
        container_name = "platinumlogs"

        local_path = "./"
        upload_file_path = os.path.join(local_path, filetoupload)

        blob_client = blob_service_client.get_blob_client(container=container_name, blob=filetoupload)

        print("\nUploading to Azure Storage as blob:\n\t" + filetoupload)

        with open(upload_file_path, "rb") as data:
            blob_client.upload_blob(data)

    except Exception as ex:
        print('Exception:')
        print(ex)

def main():
    s3 = boto3.resource('s3')
    
    BUCKET = 'logs-i-platinum' ###  s3 bucket name
    
    year = str(datetime.date.today().year)
    month = str(datetime.date.today().month) 
    month = str(month).zfill(2) ### save the month as 04, not 4
    day = str(datetime.date.today().day)
    day = str(day).zfill(2) ### save the day as 04 not 4
    keyPrefix = "i-platinum"+year+"/"+month+"/"+day+"/platinum" 
    print keyPrefix
   
    try:
        for obj in s3.Bucket(BUCKET).objects.filter(Prefix = keyPrefix): ### looping through s3 objects , which have the keyPrefix only
        ### To print s3 object's timestamp converted to seconds from epoch
            logtsSeconds = convertS3Timestamp(obj.last_modified)
            currenttsSeconds = convertS3Timestamp(datetime.datetime.now())
            delta = currenttsSeconds - logtsSeconds ### delta variable holds the difference in seconds between log timestamp and current time
            if (delta < 15*60): ### To check if the file is added to s3 bucket in the last 15 minutes, therefore 15*60=9000 seconds
                normalizedKey = unicodedata.normalize('NFKD', obj.key).encode('ascii','ignore') ### To convert obj.key from unicode to ascii string
                normalizedKey = normalizedKey.replace('/', '-') ### strip the '/' character to be '-' since it creates confusion when saving the file locally
                print "this log should be downloaded because it's new!"
                print obj.key
                os.chdir("/opt")
                s3.Bucket(BUCKET).download_file(obj.key, normalizedKey) ### This is the main operation. This downloads the file from s3 to local file system
            else:
                continue
    except botocore.exceptions.ClientError as e: ### Raise exception in case the object doesn't exist, which means a http 404 error - page not found
        if e.response['Error']['Code'] == '404':
            print ("The object does not exist!")
        else:
            raise
    
    ### Below loop will go through files ending with .gz under /opt and save them under /var/log/i-platinum/decompressed/
    ### if path changes make sure the script will have write access right to the new location
    for filename in os.listdir("/opt/"):
        if filename.endswith(".gz"): 
            print "Uploading to Azure"
            upload_to_azure(filename)
            print "decompressing file: "
            print filename
            with gzip.open(filename, 'r') as f_in, open('/var/log/i-platinum/decompressed/'+filename, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
        else:
            continue
    
    ### Below loop will go through files ending with .gz under /var/log/i-platinum/decompressed/ , save the files under /var/log/i-platinum/json/ after converting the text format to json. Also will remove .gz from file name.  
    ### Decrease the file size by deleting image including lines and convert the decompressed files to json
    os.chdir("/var/log/i-platinum/decompressed/")
    for f in os.listdir("."):
        if f.endswith(".gz"):
            print "Now decreasing the file size by deleting lines which include images..."
            with open(f, "r") as z: ### open file 'f' as readable
                lines = z.readlines()  ### copy the lines to variable 'lines'
            with open(f, "w") as z: ###open file 'f' as writeable
                for line in lines:
                    image_str = [("image" in line), (".jpg" in line), (".png" in line), (".bmp" in line), (".svg" in line)] ### truthy or falsey statement whether line contains any of image related strings
                    if any(image_str): ### if there is no image string in the opened line
                        continue
                    else:
                        z.write(line)       ### write that line that doesn't have image in it. and ignore everything else 
            print "Converting log file to json:"
            print f
            jsonconversion(f)
            os.rename(f, f[:-3])
        else:
            continue
    
    ### Below loop will go through files ending with .gz under /var/log/i-platinum/json/ and add '.json' to the file name
    os.chdir("/var/log/i-platinum/json/")
    for f in os.listdir("."):
        if not f.endswith(".json"):
            os.rename(f, (f[:-3]+ ".json"))
        else:
            continue

if __name__ == "__main__":
    main()
