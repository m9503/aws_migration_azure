FROM python:3.9-slim-buster

LABEL org.opencontainers.image.authors="Firuz Dumlupinar"

RUN mkdir -p /opt/migrator
WORKDIR /opt/migrator
COPY ./aws_migration_azure.py /opt/migrator/

RUN addgroup --gid 9000 app-user \
    && adduser -D -h /opt/migrator -u 9000 -G app-user -s /bin/sh app-user \
    && pip3 --no-cache-dir install termcolor

USER app-user

ENTRYPOINT ["python3"]

CMD ["python3", "/opt/migrator/aws_migration_azure.py"]
